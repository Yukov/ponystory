﻿using UnityEngine;
using System.Collections.Generic;

public class Sheferd : Actor
{
    [SerializeField]
    private FollowerContainer    _followers = null;
    [SerializeField]
	private Transform	    _selected = null;
    [SerializeField]
	private LayerMask	    _followerLayer = new LayerMask();
    [SerializeField]
	private float		    _radius = 0;

    private bool            _active = false;

	void Update()
	{
	    if (!_active)
	        return;

        var colliders = Physics.OverlapSphere(transform.position, _radius, _followerLayer);

        foreach (var c in colliders)
        {
            var target = c.gameObject.GetComponent<Follower>();

            if (target!=null)
                _followers.AddFollower(target);
        }
    }

    public bool Select()
    {
        _active = true;
        _selected.gameObject.SetActive(true);
		return true;
	}

	public void UnSelect()
	{
	    _active = false;
        _selected.gameObject.SetActive(false);
	    FreeSlots();
	}

    private void FreeSlots()
    {
        _followers.Free();
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(0, 1, 0, 0.3f);
        Gizmos.DrawSphere(transform.position, _radius);
    }
}
