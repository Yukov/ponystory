﻿using UnityEngine;
using System.Collections;

public class Corral : MonoBehaviour {

    [SerializeField]
    private FollowerContainer   _followers = null;
    [SerializeField]
    private LayerMask           _followerLayer = new LayerMask();
    [SerializeField]
    private Vector2             _size;

	void Update ()
    {
        var colliders = Physics.OverlapBox(transform.position, new Vector3(_size.x/2, 5, _size.y/2), Quaternion.identity, _followerLayer);

        foreach (var c in colliders)
        {
            var target = c.gameObject.GetComponent<Follower>();
            if (target!=null)
                _followers.AddFollower(target);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(0, 1, 0, 0.3f);
        Gizmos.DrawCube(transform.position, new Vector3(_size.x, 1, _size.y));
    }
}
