﻿using System;
using UnityEngine;
using System.Collections;

public class Follower : Actor
{
    private Transform   _target = null;
    private int         _targetPriority = 0;
    private Action<Follower> _onStopFollowing;

    public bool Busy
    {
        get { return _target != null; }
    }

    public int TargetPriority
    {
        get { return _targetPriority; }
    }

    public void StartFollow(Transform target, int targetPriority, Action<Follower> onStop=null)
    {
        if (_onStopFollowing!=null)
            _onStopFollowing(this);
        _target = target;
        _targetPriority = targetPriority;
        _onStopFollowing = onStop;
    }

    public void StopFollow()
    {
        _onStopFollowing = null;
        _target = null;
    }


    void Update ()
    {
	    if( Busy )
            SetTarget(_target.position);
	}
}
