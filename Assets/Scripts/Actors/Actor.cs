﻿using UnityEngine;
using System.Collections;

public class Actor : MonoBehaviour {

    private NavMeshAgent _navMeshAgent;

    virtual protected void Start()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
    }

    public void SetTarget(Vector3 target)
    {
        _navMeshAgent.destination = target;
    }

}
