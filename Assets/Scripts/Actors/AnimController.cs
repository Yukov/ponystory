﻿using UnityEngine;
using System.Collections;

public class AnimController : MonoBehaviour {

	enum States
	{
		IdleUp,
		IdleDown,
		IdleRight,
		IdleLeft,

		MoveUp,
		MoveDown,
		MoveRight,
		MoveLeft,
	}

	private static readonly string StopTrigger = "Stop";

	private NavMeshAgent	_navMeshAgent;
	private Animator		_animator;

	private States			_curState = States.IdleDown;

	void Start () 
	{
		_animator = GetComponent<Animator>();
		_navMeshAgent = GetComponent<NavMeshAgent>();
	}
	
	void Update () 
	{
		if( _navMeshAgent.hasPath )
		{
			var dif = _navMeshAgent.steeringTarget - transform.position;

			var vertical = Mathf.Abs(dif.z) > Mathf.Abs(dif.x);
			var positive = vertical ? dif.z > 0 : dif.x > 0;
			var nextState = States.IdleLeft + (vertical ? 0 : 2) + (positive ? 1 : 2);
			SetState((States)nextState);
		}
		else if( _curState>States.IdleLeft)
		{
			var nextState = _curState - States.MoveUp;
			SetState((States)nextState);
		}
	}

	void SetState(States state)
	{
		if (_curState == state)
			return;

		if (_curState > States.IdleLeft || state <= States.IdleLeft)
			_animator.SetTrigger(StopTrigger);

		_curState = state;


		if(state>States.IdleLeft)
		{
			var trigger = _curState.ToString();
			_animator.SetTrigger(trigger);
			//Debug.Log("===== Set Current state = " + state.ToString() + ", trigger - " + trigger);
		}
	}
}
