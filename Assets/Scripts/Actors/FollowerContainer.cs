﻿using UnityEngine;
using System.Collections.Generic;

public class FollowerContainer : MonoBehaviour {

    class FollowerSlot
    {
        public Follower _follower;
        public Transform _slot;
        public bool _busy;
    };

    [SerializeField]
    private Transform   _slotContainer = null;
    [SerializeField]
    private int         _priority = 0;


    private List<FollowerSlot> _slots = new List<FollowerSlot>();

    void Start ()
    {
        foreach (Transform slot in _slotContainer)
        {
            _slots.Add(new FollowerSlot() { _slot = slot, _busy = false, _follower = null });
        }
    }

    private void OnStopFollowing(Follower follower)
    {
        foreach (var slot in _slots)
        {
            if (slot._follower)
                slot._busy = false;
        }
    }

    public void AddFollower(Follower target)
    {
        if (target.Busy && target.TargetPriority>=_priority)
            return;

        FollowerSlot found = null;
        float dist = float.MaxValue;
        foreach (var slot in _slots)
        {
            if (slot._busy)
                continue;

            var d = (slot._slot.position - target.transform.position).sqrMagnitude;
            if (d < dist)
            {
                found = slot;
                d = dist;
            }
        }

        if (found != null)
        {
            found._busy = true;
            found._follower = target;
            target.StartFollow(found._slot, _priority, OnStopFollowing);
        }
    }

    public void Free()
    {
        foreach (var slot in _slots)
        {
            if (slot._busy)
            {
                slot._follower.StopFollow();
                slot._busy = false;
            }
        }
    }
}
