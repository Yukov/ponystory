using System.Collections.Generic;
using UnityEngine;

public class GameRoot : MonoBehaviour
{
    private Transform           _ingameMenu = null;
    [SerializeField]
    private Transform           _pauseMenu = null;

	private PlayerController	_player;

    public Transform IngameMenu
    {
        get { return _ingameMenu; }
    }

	public PlayerController Player
    {
        set { _player = value; }
        get { return _player; }
    }

    private void Awake()
	{
		Game.Set.GameRoot = this;
	}

	private void Start()
	{
        Game.CurrentState = States.Game;
    }

    private void OnEnable()
    {
    }

    private void Update( )
    {

    }

	private void OnDestroy( )
	{
        Game.Set.GameRoot = null;
	}

    public void OnPause()
    {
        _pauseMenu.gameObject.SetActive(true);
    }

    public void OnUnPause()
    {
        _pauseMenu.gameObject.SetActive(false);
    }

    public void OnRestart()
    {
        Game.States.SetActiveState(new GameState());
    }

}
