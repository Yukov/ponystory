﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	private readonly float ClickWait = 0.2f;

	[SerializeField]
	private Texture		_rectTexture = null;
	[SerializeField]
	private TargetManager _targetManager = null;

	private Vector3		_startPosition;
	private bool		_selecting;
	private float 		_clickTime;

	public void OnEnable()
	{
		Game.GameRoot.Player = this;
	}

	public void OnDisable()
	{
		Game.GameRoot.Player = null;
	}

	void Update()
	{
		if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
		{
			return;
		}

		if (Input.GetMouseButtonDown(0))
		{
			_startPosition = Input.mousePosition;
			_clickTime = Time.time;
			_selecting = true;
			_targetManager.StartSelection(_startPosition);
		}

		if (Input.GetMouseButtonUp(0))
		{
			_selecting = false;

			bool click = ((Time.time - _clickTime) < ClickWait);
			_targetManager.FinishSelection(Input.mousePosition, click);
		}

		if (_selecting )
		{
			_targetManager.Select(Input.mousePosition);
		}
	}

	void OnGUI()
	{
		if (_selecting)
		{
			var screenPosition1 = _startPosition;
			var screenPosition2 = Input.mousePosition;

			screenPosition1.y = Screen.height - screenPosition1.y;
			screenPosition2.y = Screen.height - screenPosition2.y;
			var topLeft = Vector3.Min(screenPosition1, screenPosition2);
			var bottomRight = Vector3.Max(screenPosition1, screenPosition2);
			var rect = Rect.MinMaxRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);

			GUI.color = new Color(1,1,1,0.2f);
			GUI.DrawTexture(rect, _rectTexture, ScaleMode.StretchToFill);
			GUI.color = Color.white;
		}
	}
}
