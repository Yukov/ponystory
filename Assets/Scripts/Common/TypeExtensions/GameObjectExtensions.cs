using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtensions
{
	public static void SetLayerRecursively(this GameObject selfObject, int layer)
	{
		selfObject.layer = layer;
		foreach (Transform t in selfObject.transform)
		{
			t.gameObject.SetLayerRecursively(layer);
		}
	}
	public static void SwitchLayerRecursively(this GameObject selfObject, int currLayer, int newLayer)
	{
		if (selfObject.layer == currLayer)
		{
			selfObject.layer = newLayer;
		}

		foreach (Transform t in selfObject.transform)
		{
			t.gameObject.SwitchLayerRecursively(currLayer, newLayer);
		}
	}

	public static T GetInterface<T>(this GameObject @this) where T : class
	{
		return @this.GetComponent<T>();
	}
	public static T GetOrAddComponent<T>(this GameObject @this) where T : Component
	{
		return @this.GetComponent<T>() ?? @this.AddComponent<T>();
	}

	public static T GetOrAddComponent<T>(this Component @this) where T : Component
	{
		return @this.GetComponent<T>() ?? @this.gameObject.AddComponent<T>();
	}
}
