﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class Utils
{
	public static void ShuffleList<T>(this IList<T> list)  
	{  
		System.Random rng = new System.Random();  
		int n = list.Count;  
		while (n > 1) {  
			n--;  
			int k = rng.Next(n + 1);  
			T value = list[k];  
			list[k] = list[n];  
			list[n] = value;  
		}  
	}

    public static void SetX(Transform t, float x)
    {
        Vector3 p = t.position; 

        t.position = new Vector3(x, p.y, p.z);
    }

    public static void SetY(Transform t, float y)
    {
        Vector3 p = t.position;

        t.position = new Vector3(p.x, y, p.z);
    }

    public static void SetZ(Transform t, float z)
    {
        Vector3 p = t.position;

        t.position = new Vector3(p.x, p.y, z);
    }

    public static void SetMParamX(GameObject obj, String param, float x)
    {
        Renderer renderer = obj.GetComponent<Renderer>();
        Material material = renderer.materials[0];
        
        Vector3 p = material.GetVector(param);
        material.SetVector(param, new Vector3(x, p.y, p.z));
    }

    public static void SetMParamY(GameObject obj, String param, float y)
    {
        Renderer renderer = obj.GetComponent<Renderer>();
        Material material = renderer.materials[0];

        Vector3 p = material.GetVector(param);
        material.SetVector(param, new Vector3(p.x, y, p.z));
    }

    public static void SetMParamZ(GameObject obj, String param, float z)
    {
        Renderer renderer = obj.GetComponent<Renderer>();
        Material material = renderer.materials[0];

        Vector3 p = material.GetVector(param);
        material.SetVector(param, new Vector3(p.x, p.y, z));
    }

    public static void SetMParamR(GameObject obj, String param, float r)
    {
        Renderer renderer = obj.GetComponent<Renderer>();
        Material material = renderer.materials[0];

        Color p = material.GetColor(param);
        material.SetColor(param, new Color(r, p.g, p.b, p.a));
    }

    public static void SetMParamG(GameObject obj, String param, float g)
    {
        Renderer renderer = obj.GetComponent<Renderer>();
        Material material = renderer.materials[0];

        Color p = material.GetColor(param);
        material.SetColor(param, new Color(p.r, g, p.b, p.a));
    }

    public static void SetMParamB(GameObject obj, String param, float b)
    {
        Renderer renderer = obj.GetComponent<Renderer>();
        Material material = renderer.materials[0];

        Color p = material.GetColor(param);
        material.SetColor(param, new Color(p.r, p.g, b, p.a));
    }

    public static void SetMParamA(GameObject obj, String param, float a)
    {
        Renderer renderer = obj.GetComponent<Renderer>();
        Material material = renderer.materials[0];

        Color p = material.GetColor(param);
        material.SetColor(param, new Color(p.r, p.g, p.b, a));
    }

    public static void SetScaleX(GameObject obj, float x)
    {
        Vector3 p = obj.transform.localScale;

        obj.transform.localScale = new Vector3(x, p.y, p.z);
    }

    public static void SetScaleY(GameObject obj, float y)
    {
        Vector3 p = obj.transform.localScale;

        obj.transform.localScale = new Vector3(p.x, y, p.z);
    }

    public static void SetScaleZ(GameObject obj, float z)
    {
        Vector3 p = obj.transform.localScale;

        obj.transform.localScale = new Vector3(p.x, p.y, z);
    }

    public static void SetEulerX(GameObject obj, float x)
    {
        Vector3 p = obj.transform.eulerAngles;

        obj.transform.eulerAngles = new Vector3(x, p.y, p.z);
    }

    public static void SetEulerY(GameObject obj, float y)
    {
        Vector3 p = obj.transform.eulerAngles;

        obj.transform.eulerAngles = new Vector3(p.x, y, p.z);
    }

    public static void SetEulerZ(GameObject obj, float z)
    {
        Vector3 p = obj.transform.eulerAngles;

        obj.transform.eulerAngles = new Vector3(p.x, p.y, z);
    }

    static public List<Transform> getChildrenInObject(GameObject fromGameObject, bool includeInactive)
    {
        List<Transform> childs = new List<Transform>();
        Transform[] ts = fromGameObject.transform.GetComponentsInChildren<Transform>(includeInactive);
        foreach (Transform t in ts)
        {
            if (t.parent.gameObject == fromGameObject)
                childs.Add(t);
        }

        return childs;
    }

    static public void setActiveRecursively(Transform from, bool active, bool includeSelf)
    {
        Transform[] ts = from.GetComponentsInChildren<Transform>(true);
        foreach (Transform t in ts)
        {
            if (t != from || includeSelf)
                t.gameObject.SetActive(active);
        }
    }

    static public Quaternion LookAt2D(Transform transform, Vector3 position)
    {
        float angleRad = Mathf.Atan2(transform.position.y - position.y, transform.position.x - position.x) + Mathf.PI / 2f;
        float angleDeg = (180 / Mathf.PI) * angleRad;
        return Quaternion.Euler(0, 0, angleDeg);
    }
}
