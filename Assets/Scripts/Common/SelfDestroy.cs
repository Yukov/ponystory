﻿using UnityEngine;
using System.Collections;

public class SelfDestroy : MonoBehaviour {
	
	public float timeToDestroy = 1;
	// Use this for initialization
	void Start () {
		Destroy(gameObject, timeToDestroy);
	}

	/*
	void DestroyChildGameObject ()
	{
		// Destroy this child gameobject, this can be called from an Animation Event.
		if(transform.Find(namedChild).gameObject != null)
			Destroy (transform.Find(namedChild).gameObject);
	}
	
	void DisableChildGameObject ()
	{
		// Destroy this child gameobject, this can be called from an Animation Event.
		if(transform.Find(namedChild).gameObject.activeSelf == true)
			transform.Find(namedChild).gameObject.SetActive(false);
	}*/
	
	void DestroyGameObject ()
	{
		// Destroy this gameobject, this can be called from an Animation Event.
		Destroy (gameObject);
	}
}
