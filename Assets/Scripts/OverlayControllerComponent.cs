﻿using UnityEngine;
using System.Collections;


public class OverlayControllerComponent : MonoBehaviour
{
    public Transform Console;
    public Transform Touches;

	public float TOGGLE_TIME = 1;

    private bool _hold = false;
    private float _holdTime = 0;

	void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}

	void Update()
    {
        if (_hold)
        {
			if (!CheckMousePosition())
			{
				_holdTime = 0;
				_hold = false;

				return;
			}

            _holdTime += Time.deltaTime;

            if (_holdTime >= TOGGLE_TIME)
            {
                _holdTime = 0;
                _hold = false;

                ToggleConsole();
            }
        }
    }

    public void ToggleConsole()
    {
        Console.gameObject.SetActive(!Console.gameObject.activeSelf);
    }

    public void ToggleTouches()
    {
        Touches.gameObject.SetActive(!Touches.gameObject.activeSelf);
    }

    public ConsoleController GetConsoleController()
    {
        return Console.GetComponent<ConsoleController>();
    }

	bool CheckMousePosition()
	{
		return Input.mousePosition.x < 100 && Input.mousePosition.y < 100;
	}

	void OnGUI()
	{
		switch (Event.current.type)
		{
			case EventType.MouseDown:
				if (CheckMousePosition())
				{
					_hold = true;
				}
				break;

			case EventType.MouseUp:
				_hold = false;
				break;

			case EventType.KeyDown:
				if (Event.current.keyCode == KeyCode.BackQuote)
				{
					ToggleConsole();
				}
				break;
		}

        if (Event.current.keyCode == KeyCode.BackQuote)
        {
            Event.current.Use();
        }
    }
}
