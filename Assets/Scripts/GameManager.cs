using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

//[ExplicitCallBindings]
public class GameManager : MonoBehaviour
{
    [SerializeField]
    private AsyncOperation _asyncOperation;

    public Boolean IsSceneLoading
    {
        get
        {
            return _asyncOperation != null;
        }
    }

    public float SceneLoadingProgress
    {
        get
        {
            return _asyncOperation == null ? 100 : _asyncOperation.progress * 100;
        }
    }

    public void LoadLevel(States level)
    {
        StopAllCoroutines();
        StartCoroutine(LoadLevelAsync(level.ToString()));
    }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        // If no GameManager ever existed, we are it.
        if (Game.Manager == null)
        {
            Game.Set.Manager = this;
        }
        else if (Game.Manager != this) // If one already exist, it's because it came from another level.
        {
            DestroyImmediate(gameObject);
            return;
        }

        if (!Application.runInBackground)
        {
            Application.runInBackground = true;
        }

        try
        {
            if (!Game.Initialized)
            {
                Game.Init();
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    private Boolean IsIphone
    {
        get
        {
#if UNITY_IOS || UNITY_IPHONE
		if (UnityEngine.iOS.Device.generation.ToString().IndexOf("iPhone") > -1)
		{
			return true;
		}
#endif
            return false;
        }
    }

    private void Start()
    {
        Application.backgroundLoadingPriority = ThreadPriority.Low;
    }

    private void Update()
    {
        if (!Game.Initialized)
        {
            return;
        }

        Game.Update(Time.deltaTime);

        if (IsSceneLoading)
        {
            if (_asyncOperation.progress >= 0.9f && !_asyncOperation.allowSceneActivation)
            {
                _asyncOperation.allowSceneActivation = true;
            }
        }
        
    }

    private void OnApplicationQuit()
    {
        Game.Destroy();
    }

    private IEnumerator LoadLevelAsync(String level)
    {
        yield return null;

        GC.Collect();

        _asyncOperation = SceneManager.LoadSceneAsync(level);
        _asyncOperation.allowSceneActivation = false;
        yield return _asyncOperation;

        _asyncOperation = null;
        yield return null;
    }

}
