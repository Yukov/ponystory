using UnityEngine;

#pragma warning disable 649

public class MenuRoot : MonoBehaviour
{
    [SerializeField]
    private Transform       _menuPage;

    public Transform MenuPage
    {
        get
        {
            return _menuPage;
        }
    }

    private void Awake()
	{
		Game.Set.MenuRoot = this;
	}

	private void Start()
	{
		Game.CurrentState = States.Menu;

        MenuPage.gameObject.SetActive(true);
    }
		
    private void Update( )
	{

	}

	private void OnDestroy( )
	{
        Game.Set.MenuRoot = null;
	}

    public void StartGame()
    {
        Game.States.SetActiveState(new GameState());
    }
}
