﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;

public class GameState : IState
{
    private Vector3     _gizmoMouseTarget;

    private bool        _paused = false;
    private bool        _prevDown = false;

    public bool Paused 
    {
        set
        {
            if (value)
                Game.GameRoot.OnPause();
            else
                Game.GameRoot.OnUnPause();

            _paused = value;
        }
        get { return _paused; }
    }

    #region IState
	public States Type
    {
        get
        {
            return States.Game;
        }
    }

    public void Active()
    {
        Game.Manager.LoadLevel(States.Game);
    }

    public void Deactive()
    {
    }

	public void Update ( )
	{

	}

    public bool InPause()
    {
        return _paused;
    }
    #endregion


    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(_gizmoMouseTarget, 0.3f);
        //Debug.Log(gizmoMouseTarget);
    }
}
