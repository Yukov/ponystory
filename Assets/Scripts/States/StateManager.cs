﻿using System.Collections.Generic;
using UnityEngine;

public class StateManager
{
    private IState _currentState;

    public void Init()
    {
        _currentState = new InitializingState();
        _currentState.Active();
    }

    public IState CurrentState
    {
        get
        {
            return _currentState;
        }
    }

    public MenuState MenuState
    {
        get
        {
            return _currentState as MenuState;
        }
    }

    public GameState GameState
    {
        get
        {
            return _currentState as GameState;
        }
    }

    public bool IsMenuState
    {
        get
        {
            return CurrentState.Type == States.Menu;
        }
    }

    public bool IsSpaceState
    {
        get
        {
            return CurrentState.Type == States.Game;
        }
    }

    public void SetActiveState(IState newState)
    {
        if (newState == _currentState)
        {
            return;
        }
        Debug.Log("Switching state from " + _currentState.Type + " to " + newState.Type);

        _currentState.Deactive();
        _currentState = newState;
        _currentState.Active();
    }

    public void Update()
    {
        if (_currentState != null)
            _currentState.Update();
    }
}
