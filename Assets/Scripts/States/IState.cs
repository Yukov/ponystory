﻿using System;

public interface IState
{
    States Type { get; }

    void Active();
    void Deactive();
    void Update();

    bool InPause();
}
