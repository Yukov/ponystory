﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitializingState : IState
{
    #region IState
    public States Type
    {
        get { return States.Initializing; }
    }

    public void Active()
    {
        var sceneName = SceneManager.GetActiveScene().name;
        if (sceneName != States.Initializing.ToString())
        {
            Game.Manager.LoadLevel(States.Initializing);
        }
           
        var menuState = new MenuState();
        Game.States.SetActiveState(menuState);
    }

    public void Deactive()
    {
    }

	public void Update ( )
	{
		    
	}

    public bool InPause()
    {
        return false;
    }

	#endregion
}
