﻿using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameInit : MonoBehaviour
{
    private void Awake()
    {
        if (Game.Manager != null)
        {
            DestroyImmediate(gameObject);
            return;
        }

        var sceneName = SceneManager.GetActiveScene().name;
        Debug.Log("[GameInit] - Start From: " + sceneName);

        if (sceneName == States.Initializing.ToString())
            return;

        Debug.Log("[GameInit] - Load Init Level: " + States.Initializing);

        var tr = transform;
        var roots = FindObjectsOfType<Transform>().Where(t => t.parent == null && t != tr).ToList();
        foreach (var rootObject in roots)
        {
            DestroyImmediate(rootObject.gameObject);
        }

        SceneManager.LoadScene(States.Initializing.ToString());
    }
}
