﻿using System;
using System.Collections.Generic;

using UnityEngine;

public class MenuState : IState
{
    #region IState

	public States Type
    {
        get
        {
            return States.Menu;
        }
    }

    public void Active()
    {
        Game.Manager.LoadLevel(States.Menu);
    }

    public void Deactive()
    {
    }

	public void Update ( )
	{

	}

    public bool InPause()
    {
        return false;
    }

    #endregion
}
