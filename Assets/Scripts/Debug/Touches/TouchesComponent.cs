﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TouchesComponent : MonoBehaviour 
{

    private Camera overlayCamera;
    private List<TouchComponent> _touches;

    void OnEnable()
    {
        _touches = new List<TouchComponent>(GetComponentsInChildren<TouchComponent>(true));
        GameObject overlayCameraObj =  GameObject.Find("OverlayCamera");
        if (overlayCameraObj)
        {
            overlayCamera = overlayCameraObj.GetComponent<Camera>();
        }

    }
    

    private void UpdateTouch(int i, Vector3 mousePos)
    {
        if (overlayCamera != null)
        {
            Vector3 pos = overlayCamera.ScreenToWorldPoint(mousePos);

            pos.z = 0;

            if (_touches.Count <= i)
            {
                return;
            }

            _touches[i].OnTouch(pos);
        }
    }


	// Update is called once per frame
	void Update () 
    {
        if (_touches.Count < 0)
        {
            return;
        }

#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            
            UpdateTouch(0, Input.mousePosition);
            
        }
#else
        
        for (int i = 0; i < Input.touchCount && i < _touches.Count; i++)
        {
            UpdateTouch(i, Input.touches[i].position);
        }
#endif

    }
}
