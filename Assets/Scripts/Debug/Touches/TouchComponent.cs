﻿using UnityEngine;
using System.Collections;

public class TouchComponent : MonoBehaviour 
{
    private const float TOUCH_DISAPPEAR_DELAY = 0.3f;

    public void OnTouch(Vector3 pos)
    {
        if(!gameObject.activeSelf)
        {
            gameObject.SetActive(true);
        }

        StopAllCoroutines();
        pos.z = transform.position.z;
        transform.position = pos;
        StartCoroutine(TouchCoroutine());
    }

    private IEnumerator TouchCoroutine()
    {
        yield return new WaitForSeconds(TOUCH_DISAPPEAR_DELAY);
        gameObject.SetActive(false);

    }
}
