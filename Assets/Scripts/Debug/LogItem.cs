﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

#pragma warning disable 649

public class LogItem : MonoBehaviour
{

	[SerializeField]
	private Text _text;
	[SerializeField]
	private Transform[] _logTypes;

	private ConsoleController.LogEntry _logEntry;

	public event Action<ConsoleController.LogEntry> OnItemClick;

	public ConsoleController.LogEntry LogEntry
	{
		get { return _logEntry; }
	}

	public void Init(ConsoleController.LogEntry entry)
	{
		_logEntry = entry;
		_text.text = entry.Message;

		var values = LogType.GetValues(typeof(LogType));
		foreach (LogType type in values)
		{
			var s = _logTypes[(int)type];
			s.gameObject.SetActive(entry.Type == type);
		}
	}

	public void OnClick()
	{
		OnItemClick(_logEntry);
	}
}
