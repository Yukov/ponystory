﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

#pragma warning disable 649

public class ConsoleController : MonoBehaviour
{
	const int MaxLogItemCount = 100;
	[SerializeField]
	private ScrollRect _scrollOutput;
	[SerializeField]
	private Text _itemDesc;
	[SerializeField]
	private LogItem _logItemPrefab;

	public struct LogEntry
	{
		public float Time;
		public LogType Type;
		public String Message;
		public String Details;
	}

	public void InitConsole()
	{
		//Application.RegisterLogCallback(LogHandler);
		Application.logMessageReceived += LogHandler;
	}

	private void OnDestroy()
	{
		Application.logMessageReceived -= LogHandler;
	}

	private void LogHandler(string message, string stacktrace, LogType type)
	{
		int count = _scrollOutput.content.childCount;

		LogItem item;
		if (count < MaxLogItemCount)
		{
			item = Instantiate(_logItemPrefab) as LogItem;
			item.OnItemClick += OnLogItemClick;
		}
		else
		{
			item = _scrollOutput.content.GetChild(0).GetComponent<LogItem>() as LogItem;
			item.transform.SetAsLastSibling();
		}

		LogEntry entry = new LogEntry();
		entry.Time = Time.realtimeSinceStartup;
		entry.Type = type;
		entry.Message = message;
		entry.Details = stacktrace;

		item.Init(entry);
		item.transform.SetParent(_scrollOutput.content, false);

		var tolerance = 0.01f;
		if (Mathf.Abs(_scrollOutput.normalizedPosition.y) < tolerance)
			StartCoroutine(ScrollOutputToBottom());
	}

	void OnLogItemClick(LogEntry entry)
	{
		var minutes = (int)entry.Time / 60;
		var seconds = entry.Time % 60;

		_itemDesc.text = string.Format("{0:00}:{1:00.00} \n{2} \n\n{3}", minutes, seconds, entry.Message, entry.Details);
	}

	public void OnClose()
	{
		gameObject.SetActive(false);
	}

	IEnumerator ScrollOutputToBottom()
	{
		yield return new WaitForEndOfFrame();
		_scrollOutput.normalizedPosition = new Vector2(0, 0);
	}


}
