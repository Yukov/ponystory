﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TargetManager : MonoBehaviour 
{
	[SerializeField]
	private LayerMask			_interactiveLayer = new LayerMask();
	[SerializeField]
	private Transform			_crossPrefab = null;

	private List<Sheferd>	    _selections = new List<Sheferd>();
	private Plane				_groundPlane;
	private Vector3				_mouseStart;

	private List<Sheferd>	    _selectionCache = new List<Sheferd>();

	void Start ()
	{
		_groundPlane = new Plane(Vector3.up, Vector3.zero);
	}
	
	public void StartSelection(Vector3 mouseStart)
	{
		_mouseStart = mouseStart;
	}

	public void FinishSelection(Vector3 mouseEnd, bool click)
	{
		var selections = UpdateSelection(_mouseStart, mouseEnd);

		if(selections.Count>0)
		{
			SetSelections(selections);
		}
		else if(click)
		{
			SetTarget(mouseEnd);
		}
	}

	public void Select(Vector3 mouseEnd)
	{
		var selections = UpdateSelection(_mouseStart, mouseEnd);
		if(selections.Count>0)
			SetSelections(selections);
	}

	List<Sheferd> UpdateSelection(Vector3 mouseStart, Vector3 mouseEnd)
	{
		_selectionCache.Clear();

		var p1 = GroundPoint(mouseStart);
		var p2 = GroundPoint(mouseEnd);
		var center = (p1 + p2) / 2;
		var halfExtents = (p1 - p2) / 2; halfExtents = new Vector3(Mathf.Abs(halfExtents.x), Mathf.Abs(halfExtents.y), Mathf.Abs(halfExtents.z));

		var colliders = Physics.OverlapBox(center, halfExtents, Quaternion.identity, _interactiveLayer);
		foreach (var c in colliders)
		{
			var target = c.gameObject.GetComponent<Sheferd>();
			if (target != null)
				_selectionCache.Add(target);
		}

		//Debug.Log(string.Format("===UpdateSelection - p1={0}, p2={1}, center={2}, halfExtents={3}, count={4}", p1, p2, center, halfExtents, _selectionCache.Count));
		return _selectionCache;
	}

	void SetSelections(List<Sheferd> selections)
	{
		_selections.ForEach(s => s.UnSelect());
		_selections.Clear();
		_selections.AddRange(selections);
		_selections.ForEach(s => s.Select());
	}

	void SetTarget(Vector3 mouseEnd)
	{
		var p = GroundPoint(mouseEnd);
	    var count = _selections.Count;
	    var part = 1f/count;
	    var radius = 0.4f*(count - 1);
	    var startAngle = Random.Range(0, Mathf.PI);
		for (int i=0; i<count; i++)
		{
		    var s = _selections[i];
            var angle = part * i * Mathf.PI*2 + startAngle;
            var offset = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle));
		    offset *= radius;
		    var tp = p + offset;
			s.SetTarget(tp);
            Instantiate(_crossPrefab, tp, Quaternion.identity);
        }
	}

	Vector3 GroundPoint(Vector3 mousePos)
	{
		var ray = Camera.main.ScreenPointToRay(mousePos);
		var distance = 0f;
		_groundPlane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}
}
