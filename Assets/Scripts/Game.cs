using System;
using System.Collections.Generic;

using UnityEngine;

using Object = UnityEngine.Object;

public static class Game
{
    private static GameManager  _gameManager;
    private static MenuRoot     _menuRoot;
    private static GameRoot    _gameRoot; 

        
    private static String		_lastError = "";

    public static Boolean Initialized
    {
        get
        {                return States != null;
        }
    }
    public static String LastError
    {
        get
        {
            return _lastError;
        }
        set
        {
            _lastError = value;
        }
    }
    public static GameManager Manager
    {
        get
        {
            return _gameManager;
        }
    }

    public static StateManager States
    {
        private set;
        get;
    }
        
    public static MenuState MenuState
    {
        get
        {
            return States.MenuState;
        }
    }

    public static States CurrentState
    {
        get;
        set;
    }

	public static ConsoleController Console
	{
		private set;
		get;
	}

    public static GameRoot GameRoot
    {
        get { return _gameRoot; }
    }

    public static void Init()
    {
        //State manager initializaion
        if (Initialized)
        {
            throw new Exception("Game repository already initialized!");
        }

		var overlayController = UnityEngine.Object.FindObjectOfType<OverlayControllerComponent>();
		if (overlayController != null)
		{
			Console = overlayController.GetConsoleController();
			Console.InitConsole();
		}

        Game.States = new StateManager();
        Game.States.Init();
    }

    public static void Update(Single deltaTime)
    {
        Game.States.Update();
    }

    public static void Destroy()
    {
        // todo: cleanup
    }

    internal static class Set
    {
        public static GameManager Manager
        {
            set
            {
                _gameManager = value;
            }
        }

        public static MenuRoot MenuRoot
        {
            set
            {
                _menuRoot = value;
            }
        }

        public static GameRoot GameRoot
        {
            set
            {
                _gameRoot = value;
            }
        }
            
    }
}
